package cjj.listener;


/**
 * @author chenjunjie
 * @since 2018-05-14
 */
public interface ButtonClickListener extends MyListener {
    /**
     * 鼠标点击按钮事件接口
     * @param e 事件对象
     */
    void buttonClicked(ButtonClickEvent e);

    /**
     * 点击按钮，按钮变色
     * @param e 事件对象
     */
    void changeButtonColor(ButtonClickEvent e);
}
