package cjj.listener;

/**
 * 简单模拟一下button点击事件
 * @author chenjunjie
 * @since 2018-05-14
 */
public class ButtonClickEvent extends MyEvent{
    /**
     * 设置事件源（“事件的寄宿对象”）
     * @param source
     */
    public ButtonClickEvent(Object source) {
        super(source);
    }

    public void alert(){
        System.out.println("提示：您点击了按钮");
    }

    public void changeColor(Button button){
        button.setColor(Color.Red.getIndex());
    }

}
