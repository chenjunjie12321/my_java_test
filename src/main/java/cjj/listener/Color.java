package cjj.listener;

/**
 * @author chenjunjie
 * @since 2018-05-15
 */
public enum Color {
    Red("red",0),
    Orange("orange",1),
    Yellow("yellow",2),
    Green("green",3),
    Bule("bule",4),
    Gray("gray",5);
    private String name;
    private int index;

    Color(String name, int i) {
        this.name = name;
        this.index = i;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static int getNIndexByName(String name) {
        for (Color c : Color.values()) {
            if (c.getName() == name) {
                return c.getIndex();
            }
        }
        return Color.Gray.getIndex();
    }

    public static String getNameByIndex(int index) {
        for (Color c : Color.values()) {
            if (c.getIndex() == index) {
                return c.getName();
            }
        }
        return Color.Gray.getName();
    }
}
