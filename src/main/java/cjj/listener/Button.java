package cjj.listener;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Button按钮
 * button中可以加入一个监听ButtonClickListener事件
 * @author chenjunjie
 * @since 2018-05-14
 */
public class Button {
    /**
     * button的名称
     */
    private String buttonName = "";

    /**
     * button的颜色, 使用了枚举, 默认灰色
     */
    private int color = Color.Gray.getIndex();

    /**
     * 事件容器（类似于观察者模式中的观察者列表）
     */
    private Set<MyListener> clickListeners = null ;

    public Button(){
        clickListeners = new HashSet<MyListener>();
        // 默认名
        this.buttonName = "button1";

    }

    public Button(String buttonName){
        clickListeners = new HashSet<MyListener>();
        this.buttonName = buttonName;
    }

    public Button(String buttonName, int color){
        this(buttonName);
        this.color = color;
    }

    /**
     * 想事件容器中加入事件
     * @param e
     */
public void addButtonClickListener(MyListener e){
        this.clickListeners.add(e);
    }

    public void ButtonClick() {
        // 通知所有监听者
        try {
            Notifies() ;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void Notifies() throws InterruptedException {
        Iterator<MyListener> iterator = clickListeners.iterator();
        // 遍历监听器列表，执行监听器的逻辑
        while (iterator.hasNext()) {
            ButtonClickListener listener = (ButtonClickListener) iterator.next();
            // 事件对象（事件中包含事件源，它俩绑定关系）
            ButtonClickEvent e = new ButtonClickEvent(this);
            listener.buttonClicked(e);
            // 如果一个监听器中有多个方法，每个方法中的事件对象的选择由设计而定
            // 这里同一个监听器使用的是同一个事件对象
            Thread.sleep(5);
            listener.changeButtonColor(e);
        }
    }

    public String getButtonName() {
        return buttonName;
    }

    public int getColorIndex() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
