package cjj.listener;

/**
 * 适配器，需要子类实现这个抽象类中的方法
 * @author chenjunjie
 * @since 2018-05-14
 */
public abstract class ButtonClickListenerAdapter implements ButtonClickListener {

    @Override
    /**
     * 鼠标点击按钮，触发事件
     */
    public void buttonClicked(ButtonClickEvent e) {}

    @Override
    /**
     * 点击
     */
    public void changeButtonColor(ButtonClickEvent e) {

    }
}
