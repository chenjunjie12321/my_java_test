package cjj.listener;

/**
 * @author chenjunjie
 * @since 2018-05-14
 */
public class MainTest {
    public static void main(String[] args) {
        Button buttonDemo = new Button("button123") ;
        // 添加监听器
        buttonDemo.addButtonClickListener(new ButtonClickListener(){

            @Override
            public void buttonClicked(ButtonClickEvent e) {
                e.alert();
                // 获取事件源
                Button source = (Button)e.getSource();
                System.out.println("按钮的名称: " + source.getButtonName());
                System.out.println("按钮的颜色:" + Color.getNameByIndex(source.getColorIndex()));
            }

            @Override
            public void changeButtonColor(ButtonClickEvent e) {
                System.out.println("-------------");
                System.out.println("改变按钮颜色...");
                Button source = (Button)e.getSource();
                source.setColor(Color.Red.getIndex());
                System.out.println("按钮的颜色:" + Color.getNameByIndex(source.getColorIndex()));
            }
        });

        // do something

        // 模拟点击按钮，事件触发
        buttonDemo.ButtonClick();
    }
}
