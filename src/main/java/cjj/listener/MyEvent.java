package cjj.listener;

/**
 * 模仿jdk中的EventObject逻辑代码
 * @author chenjunjie
 * @since 2018-05-14
 */
public class MyEvent {
    protected transient Object  source;

    public MyEvent(Object source) {
        if (source == null) {
            throw new IllegalArgumentException("null source");
        }
        this.source = source;
    }

    public Object getSource() {
        return source;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[source=" + source + "]";
    }
}
