package cjj.test;

import java.io.File;

/**
 * @author chenjunjie
 * @since 2018-01-08
 */
public class FileListDemo {
    private static String storePath = "F:\\test";

    public static void main(String[] args) {

        File dir = new File(FileListDemo.storePath);
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                System.out.printf("filename: %s\n",file.getName());
                System.out.printf("filepath: %s\n file lenght:%d \n",file.getPath(), file.length());
                System.out.println("---------");
            }
        }

    }
}
