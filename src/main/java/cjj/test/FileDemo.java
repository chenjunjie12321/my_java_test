package cjj.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author chenjunjie
 * @since 2017-10-30
 */
public class FileDemo {

    public static final String file2String(final File file) {
        if (file.exists()) {
            char[] data = new char[(int) file.length()];
            boolean result = false;

            FileReader fileReader = null;
            try {
                fileReader = new FileReader(file);
                int len = fileReader.read(data);
                result = (len == data.length);
            }
            catch (IOException e) {
                // e.printStackTrace();
            }
            finally {
                if (fileReader != null) {
                    try {
                        fileReader.close();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (result) {
                String value = new String(data);
                return value;
            }
        }
        return null;
    }




        public static void main(String[] args) {
            String content = FileDemo.file2String(new File("F:\\namesrv\\kvConfig.json"));
            System.out.println(content);

        }

}
