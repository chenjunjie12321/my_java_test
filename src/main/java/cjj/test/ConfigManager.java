package cjj.test;

import java.io.IOException;

/**
 * @author chenjunjie
 * @since 2017-10-26
 */
public abstract class ConfigManager {

    //public abstract String encode();

    public boolean load() {
        String fileName = null;

            fileName = this.configFilePath();
            System.out.println(fileName);

            return true;
    }

    public abstract String configFilePath();

}
