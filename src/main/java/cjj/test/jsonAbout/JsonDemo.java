package cjj.test.jsonAbout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by youaijj on 2018/5/5.
 */
public class JsonDemo {

    // json字符串-数组类型
    private static final String  JSON_ARRAY_STR = "[{\"studentName\":\"lily\",\"studentAge\":12},{\"studentName\":\"lucy\",\"studentAge\":15}]";
    // json字符串（普通）
    private static String studentJsonStr = "{\"age\":14,\"classNo\":1002,\"hobby\":[\"football\",\"music\",\"swim\"],\"name\":\"liming\"}";
    private static Student newAStudet(){
        Student st = new Student();
        st.setAge(1);
        st.setName("chenjunjie");
        st.setClassNo(1001);
        st.setHobby(new String[]{"basketball","music","swim"});
        return st;
    }

    public static void main(String[] args) {

        // 1.结构体转字符串
        Student st = newAStudet();
        String jsonStr = JSON.toJSONString(st);
        System.out.println(jsonStr);

        // 2.字符串转结构体
        JSONObject stuObject = JSON.parseObject(studentJsonStr);
        // get value by key
        System.out.println("name = "+stuObject.getString("name"));
        // 若果知道Json字符串对应的结构体的数据类型，则可以使用如下方式
        Student stuResult = JSON.parseObject(studentJsonStr,Student.class);
        System.out.println("name = "+stuResult.getName());

        // 3.对于json字符串-数组类型的处理
        JSONArray jsonArray = JSONArray.parseArray(JSON_ARRAY_STR);
        //遍历方式1
        int size = jsonArray.size();
        for (int i = 0; i < size; i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            System.out.println("studentName:  " + jsonObject.getString("studentName") + ":" + "  studentAge:  "
                    + jsonObject.getInteger("studentAge"));
        }
    }

}
