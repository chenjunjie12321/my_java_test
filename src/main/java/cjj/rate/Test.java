package cjj.rate;

import java.util.Arrays;

/**
 * @author junjie.chen
 * @email junjie.chen@dmall.com
 * @date 2019/1/3
 */
public class Test {
    public static void main(String[] args) {
        int month = 6;
        // 收入
        double income = 30000;
        // 税收起征点
        double baseTax = 5000;
        // 五险一金扣除金额
        double insurance = 4500;
        // 个人所得税专项附加扣除（子女教育，住房贷款，赡养老人等六项）
        double benefits = 2000;
        double discount = baseTax + insurance +benefits;
        double rateDiscount = deductMoney(income, discount, month);
        System.out.printf("The %dth month tax is %.2f", month, rateDiscount);
    }

    public static double getRate(double cumulativeIncome) {
        if (cumulativeIncome <= 36000) {
            return 0.03;
        } else if (cumulativeIncome > 36000 && cumulativeIncome <= 144000) {
            return 0.1;
        } else if (cumulativeIncome > 144000 && cumulativeIncome <= 300000) {
            return 0.2;
        } else if (cumulativeIncome > 300000 && cumulativeIncome <= 420000) {
            return 0.25;
        } else if (cumulativeIncome > 420000 && cumulativeIncome <= 6600000) {
            return 0.3;
        } else if (cumulativeIncome > 6600000 && cumulativeIncome <= 960000) {
            return 0.35;
        } else {
            return 0.45;
        }
    }

    public static int getQuickDiscountMoney(double rate) {
        if (rate == 0.03) {
            return 0;
        } else if (rate == 0.1) {
            return 2520;
        } else if (rate == 0.2) {
            return 16929;
        } else if (rate == 0.25) {
            return 31920;
        } else if (rate == 0.3) {
            return 52920;
        } else if (rate == 0.35) {
            return 85920;
        } else {
            return 181920;
        }
    }

    private static double sumTax(double[] taxs, int m) {
        double sum = 0;
        for (int i = 0; i < m; i++) {
            sum += taxs[i];
        }
        return sum;
    }

    public static double deductMoney(double income, double discount, int month) {
        int m = 0;
        double prefixMoney;
        double[] taxs = new double[12];
        double rate;
        int QuickDiscountMoney;
        while (m < 12) {
            prefixMoney = (income - discount) * (m + 1);
            rate = getRate(prefixMoney);
            QuickDiscountMoney = getQuickDiscountMoney(rate);
            taxs[m] = prefixMoney * rate - QuickDiscountMoney - sumTax(taxs, m);
            m++;
        }
        System.out.printf("Each month tax detail: %s \n", Arrays.toString(taxs));
        return taxs[month - 1];
    }
}
