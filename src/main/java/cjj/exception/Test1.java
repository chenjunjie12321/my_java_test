package cjj.exception;

/**
 * @author chenjunjie
 * @since 2018/11/4.
 */
public class Test1 {
    public static void main(String[] args) {
        try {
            test(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void test(int a) throws Exception  {

        if(a == 1){
            throw new Exception("参数越界");
            //编译错误，「无法访问的语句」
        }
        System.out.println("异常后");

    }
}
