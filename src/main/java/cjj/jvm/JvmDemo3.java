package cjj.jvm;

/**
 * @author chenjunjie
 * @since 2018-03-28
 */
public class JvmDemo3 {
    private static int a = 1;
    private static int c = 5;
    private int b = 11;
    public static void staticMethod1(){
        int i = 2;
        int j = 3;
        int m = i+j;
        int n = a;
    }

    public static void staticMethod2(){
        int i = 21;
        int j = 4;
    }

    public void method1(){
        int x = 4;
        int y = 5;
    }

}


/** 编译之后的内容
 *
Classfile /F:/cjj/cjjwork/my_java_test/output/production/my_java_test/cjj/jvm/JvmDemo3.class
  Last modified 2018-3-28; size 757 bytes
  MD5 checksum 1a70147de30c144d82daa2b113593532
  Compiled from "JvmDemo3.java"
public class cjj.jvm.JvmDemo3
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #6.#30         // java/lang/Object."<init>":()V
   #2 = Fieldref           #5.#31         // cjj/jvm/JvmDemo3.b:I
   #3 = Fieldref           #5.#32         // cjj/jvm/JvmDemo3.a:I
   #4 = Fieldref           #5.#33         // cjj/jvm/JvmDemo3.c:I
   #5 = Class              #34            // cjj/jvm/JvmDemo3
   #6 = Class              #35            // java/lang/Object
   #7 = Utf8               a
   #8 = Utf8               I
   #9 = Utf8               c
  #10 = Utf8               b
  #11 = Utf8               <init>
  #12 = Utf8               ()V
  #13 = Utf8               Code
  #14 = Utf8               LineNumberTable
  #15 = Utf8               LocalVariableTable
  #16 = Utf8               this
  #17 = Utf8               Lcjj/jvm/JvmDemo3;
  #18 = Utf8               staticMethod1
  #19 = Utf8               i
  #20 = Utf8               j
  #21 = Utf8               m
  #22 = Utf8               n
  #23 = Utf8               staticMethod2
  #24 = Utf8               method1
  #25 = Utf8               x
  #26 = Utf8               y
  #27 = Utf8               <clinit>
  #28 = Utf8               SourceFile
  #29 = Utf8               JvmDemo3.java
  #30 = NameAndType        #11:#12        // "<init>":()V
  #31 = NameAndType        #10:#8         // b:I
  #32 = NameAndType        #7:#8          // a:I
  #33 = NameAndType        #9:#8          // c:I
  #34 = Utf8               cjj/jvm/JvmDemo3
  #35 = Utf8               java/lang/Object
{
  public cjj.jvm.JvmDemo3();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: aload_0
         5: bipush        11
         7: putfield      #2                  // Field b:I
        10: return
      LineNumberTable:
        line 7: 0
        line 10: 4
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      11     0  this   Lcjj/jvm/JvmDemo3;

  public static void staticMethod1();
    descriptor: ()V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=2, locals=4, args_size=0
         0: iconst_2
         1: istore_0
         2: iconst_3
         3: istore_1
         4: iload_0
         5: iload_1
         6: iadd
         7: istore_2
         8: getstatic     #3                  // Field a:I
        11: istore_3
        12: return
      LineNumberTable:
        line 12: 0
        line 13: 2
        line 14: 4
        line 15: 8
        line 16: 12
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            2      11     0     i   I
            4       9     1     j   I
            8       5     2     m   I
           12       1     3     n   I

  public static void staticMethod2();
    descriptor: ()V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=1, locals=2, args_size=0
         0: bipush        21
         2: istore_0
         3: iconst_4
         4: istore_1
         5: return
      LineNumberTable:
        line 19: 0
        line 20: 3
        line 21: 5
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            3       3     0     i   I
            5       1     1     j   I

  public void method1();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=3, args_size=1
         0: iconst_4
         1: istore_1
         2: iconst_5
         3: istore_2
         4: return
      LineNumberTable:
        line 24: 0
        line 25: 2
        line 26: 4
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/JvmDemo3;
            2       3     1     x   I
            4       1     2     y   I

  static {};
    descriptor: ()V
    flags: ACC_STATIC
    Code:
      stack=1, locals=0, args_size=0
         0: iconst_1
         1: putstatic     #3                  // Field a:I
         4: iconst_5
         5: putstatic     #4                  // Field c:I
         8: return
      LineNumberTable:
        line 8: 0
        line 9: 4
}
SourceFile: "JvmDemo3.java"


 */