package cjj.jvm;

/**
 * JvmDemo2 Class文件字节码的工具javap测试static与多变量
 * 注意：在编译JvmDemo2.java这个文件之前要保证Hello.java已经被编译，否则会发生错误：“\Error:(9, 31) java: 找不到符号 符号: 类 Hello”
 * @author chenjunjie
 * @since 2018-03-27
 */
public class JvmDemo2 extends Hello {
    private static int a = 1;
    private int b;

    public JvmDemo2(){}

    public int functionInt(){
        int m1 = 1;
        int m2 = 3;
        int m3 = 1;
        int m4 = 3;
        int m5 = 1;
        int m6= 3;
        int m7 = 1;
        int m8 = 3;
        int m9 = 1;
        int m0 = 3;

        int p = m1 + m2 + m3 + m4 + m5 + m6 + m7 + m8 + m9 + m0;
        return p;
    }

    public String  functionString(){
        String f = "this is a test.";
        return "hello world";
    }
}


/** 反编译后的内容如下：
 *
Classfile /F:/cjj/cjjwork/my_java_test/output/production/my_java_test/cjj/jvm/JvmDemo2.class
  Last modified 2018-4-12; size 875 bytes
  MD5 checksum 3924b8e235f3d4618be9aafe102b6dd7
  Compiled from "JvmDemo2.java"
public class cjj.jvm.JvmDemo2 extends cjj.jvm.Hello
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #6.#37         // cjj/jvm/Hello."<init>":()V
   #2 = String             #38            // this is a test.
   #3 = String             #39            // hello world
   #4 = Fieldref           #5.#40         // cjj/jvm/JvmDemo2.a:I
   #5 = Class              #41            // cjj/jvm/JvmDemo2
   #6 = Class              #42            // cjj/jvm/Hello
   #7 = Utf8               a
   #8 = Utf8               I
   #9 = Utf8               b
  #10 = Utf8               <init>
  #11 = Utf8               ()V
  #12 = Utf8               Code
  #13 = Utf8               LineNumberTable
  #14 = Utf8               LocalVariableTable
  #15 = Utf8               this
  #16 = Utf8               Lcjj/jvm/JvmDemo2;
  #17 = Utf8               functionInt
  #18 = Utf8               ()I
  #19 = Utf8               m1
  #20 = Utf8               m2
  #21 = Utf8               m3
  #22 = Utf8               m4
  #23 = Utf8               m5
  #24 = Utf8               m6
  #25 = Utf8               m7
  #26 = Utf8               m8
  #27 = Utf8               m9
  #28 = Utf8               m0
  #29 = Utf8               p
  #30 = Utf8               functionString
  #31 = Utf8               ()Ljava/lang/String;
  #32 = Utf8               f
  #33 = Utf8               Ljava/lang/String;
  #34 = Utf8               <clinit>
  #35 = Utf8               SourceFile
  #36 = Utf8               JvmDemo2.java
  #37 = NameAndType        #10:#11        // "<init>":()V
  #38 = Utf8               this is a test.
  #39 = Utf8               hello world
  #40 = NameAndType        #7:#8          // a:I
  #41 = Utf8               cjj/jvm/JvmDemo2
  #42 = Utf8               cjj/jvm/Hello
{
  public cjj.jvm.JvmDemo2();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method cjj/jvm/Hello."<init>":()V
         4: return
      LineNumberTable:
        line 13: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/JvmDemo2;

  public int functionInt();
    descriptor: ()I
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=12, args_size=1
         0: iconst_1
         1: istore_1
         2: iconst_3
         3: istore_2
         4: iconst_1
         5: istore_3
         6: iconst_3
         7: istore        4
         9: iconst_1
        10: istore        5
        12: iconst_3
        13: istore        6
        15: iconst_1
        16: istore        7
        18: iconst_3
        19: istore        8
        21: iconst_1
        22: istore        9
        24: iconst_3
        25: istore        10
        27: iload_1
        28: iload_2
        29: iadd
        30: iload_3
        31: iadd
        32: iload         4
        34: iadd
        35: iload         5
        37: iadd
        38: iload         6
        40: iadd
        41: iload         7
        43: iadd
        44: iload         8
        46: iadd
        47: iload         9
        49: iadd
        50: iload         10
        52: iadd
        53: istore        11
        55: iload         11
        57: ireturn
      LineNumberTable:
        line 16: 0
        line 17: 2
        line 18: 4
        line 19: 6
        line 20: 9
        line 21: 12
        line 22: 15
        line 23: 18
        line 24: 21
        line 25: 24
        line 27: 27
        line 28: 55
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      58     0  this   Lcjj/jvm/JvmDemo2;
            2      56     1    m1   I
            4      54     2    m2   I
            6      52     3    m3   I
            9      49     4    m4   I
           12      46     5    m5   I
           15      43     6    m6   I
           18      40     7    m7   I
           21      37     8    m8   I
           24      34     9    m9   I
           27      31    10    m0   I
           55       3    11     p   I

  public java.lang.String functionString();
    descriptor: ()Ljava/lang/String;
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=2, args_size=1
         0: ldc           #2                  // String this is a test.
         2: astore_1
         3: ldc           #3                  // String hello world
         5: areturn
      LineNumberTable:
        line 32: 0
        line 33: 3
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       6     0  this   Lcjj/jvm/JvmDemo2;
            3       3     1     f   Ljava/lang/String;

  static {};
    descriptor: ()V
    flags: ACC_STATIC
    Code:
      stack=1, locals=0, args_size=0
         0: iconst_1
         1: putstatic     #4                  // Field a:I
         4: return
      LineNumberTable:
        line 10: 0
}
SourceFile: "JvmDemo2.java"

*/