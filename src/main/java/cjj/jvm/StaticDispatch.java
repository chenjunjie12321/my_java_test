package cjj.jvm;

/**
 * @author chenjunjie
 * @since 2018-04-03
 */
public class StaticDispatch {
    static abstract class Human{
    }
    static class Man extends Human{
    }
    static class Woman extends Human{
    }
    public void sayHello(Human guy){
        System.out.println("hello,guy！");
    }
    public void sayHello(Man guy){
        System.out.println("hello,gentleman！");
    }
    public void sayHello(Woman guy){
        System.out.println("hello,lady！");
    }
    public static void main(String[]args){
        Human man=new Man();
        Human woman=new Woman();
        StaticDispatch sr=new StaticDispatch();
        sr.sayHello(man);
        sr.sayHello(woman);
    }
}


/** 编译结果
 *
Classfile /F:/cjj/cjjwork/my_java_test/output/production/my_java_test/cjj/jvm/StaticDispatch.class
  Last modified 2018-4-3; size 1347 bytes
  MD5 checksum ca616b6821f0b2a2c58fbc8b7077497a
  Compiled from "StaticDispatch.java"
public class cjj.jvm.StaticDispatch
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #14.#44        // java/lang/Object."<init>":()V
   #2 = Fieldref           #45.#46        // java/lang/System.out:Ljava/io/PrintStream;
   #3 = String             #47            // hello,guy！
   #4 = Methodref          #48.#49        // java/io/PrintStream.println:(Ljava/lang/String;)V
   #5 = String             #50            // hello,gentleman！
   #6 = String             #51            // hello,lady！
   #7 = Class              #52            // cjj/jvm/StaticDispatch$Man
   #8 = Methodref          #7.#44         // cjj/jvm/StaticDispatch$Man."<init>":()V
   #9 = Class              #53            // cjj/jvm/StaticDispatch$Woman
  #10 = Methodref          #9.#44         // cjj/jvm/StaticDispatch$Woman."<init>":()V
  #11 = Class              #54            // cjj/jvm/StaticDispatch
  #12 = Methodref          #11.#44        // cjj/jvm/StaticDispatch."<init>":()V
  #13 = Methodref          #11.#55        // cjj/jvm/StaticDispatch.sayHello:(Lcjj/jvm/StaticDispatch$Human;)V
  #14 = Class              #56            // java/lang/Object
  #15 = Utf8               Woman
  #16 = Utf8               InnerClasses
  #17 = Utf8               Man
  #18 = Class              #57            // cjj/jvm/StaticDispatch$Human
  #19 = Utf8               Human
  #20 = Utf8               <init>
  #21 = Utf8               ()V
  #22 = Utf8               Code
  #23 = Utf8               LineNumberTable
  #24 = Utf8               LocalVariableTable
  #25 = Utf8               this
  #26 = Utf8               Lcjj/jvm/StaticDispatch;
  #27 = Utf8               sayHello
  #28 = Utf8               (Lcjj/jvm/StaticDispatch$Human;)V
  #29 = Utf8               guy
  #30 = Utf8               Lcjj/jvm/StaticDispatch$Human;
  #31 = Utf8               (Lcjj/jvm/StaticDispatch$Man;)V
  #32 = Utf8               Lcjj/jvm/StaticDispatch$Man;
  #33 = Utf8               (Lcjj/jvm/StaticDispatch$Woman;)V
  #34 = Utf8               Lcjj/jvm/StaticDispatch$Woman;
  #35 = Utf8               main
  #36 = Utf8               ([Ljava/lang/String;)V
  #37 = Utf8               args
  #38 = Utf8               [Ljava/lang/String;
  #39 = Utf8               man
  #40 = Utf8               woman
  #41 = Utf8               sr
  #42 = Utf8               SourceFile
  #43 = Utf8               StaticDispatch.java
  #44 = NameAndType        #20:#21        // "<init>":()V
  #45 = Class              #58            // java/lang/System
  #46 = NameAndType        #59:#60        // out:Ljava/io/PrintStream;
  #47 = Utf8               hello,guy！
  #48 = Class              #61            // java/io/PrintStream
  #49 = NameAndType        #62:#63        // println:(Ljava/lang/String;)V
  #50 = Utf8               hello,gentleman！
  #51 = Utf8               hello,lady！
  #52 = Utf8               cjj/jvm/StaticDispatch$Man
  #53 = Utf8               cjj/jvm/StaticDispatch$Woman
  #54 = Utf8               cjj/jvm/StaticDispatch
  #55 = NameAndType        #27:#28        // sayHello:(Lcjj/jvm/StaticDispatch$Human;)V
  #56 = Utf8               java/lang/Object
  #57 = Utf8               cjj/jvm/StaticDispatch$Human
  #58 = Utf8               java/lang/System
  #59 = Utf8               out
  #60 = Utf8               Ljava/io/PrintStream;
  #61 = Utf8               java/io/PrintStream
  #62 = Utf8               println
  #63 = Utf8               (Ljava/lang/String;)V
{
  public cjj.jvm.StaticDispatch();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: return
      LineNumberTable:
        line 7: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/StaticDispatch;

  public void sayHello(cjj.jvm.StaticDispatch$Human);
    descriptor: (Lcjj/jvm/StaticDispatch$Human;)V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=2
         0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
         3: ldc           #3                  // String hello,guy！
         5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
         8: return
      LineNumberTable:
        line 15: 0
        line 16: 8
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       9     0  this   Lcjj/jvm/StaticDispatch;
            0       9     1   guy   Lcjj/jvm/StaticDispatch$Human;

  public void sayHello(cjj.jvm.StaticDispatch$Man);
    descriptor: (Lcjj/jvm/StaticDispatch$Man;)V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=2
         0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
         3: ldc           #5                  // String hello,gentleman！
         5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
         8: return
      LineNumberTable:
        line 18: 0
        line 19: 8
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       9     0  this   Lcjj/jvm/StaticDispatch;
            0       9     1   guy   Lcjj/jvm/StaticDispatch$Man;

  public void sayHello(cjj.jvm.StaticDispatch$Woman);
    descriptor: (Lcjj/jvm/StaticDispatch$Woman;)V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=2
         0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
         3: ldc           #6                  // String hello,lady！
         5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
         8: return
      LineNumberTable:
        line 21: 0
        line 22: 8
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       9     0  this   Lcjj/jvm/StaticDispatch;
            0       9     1   guy   Lcjj/jvm/StaticDispatch$Woman;

  public static void main(java.lang.String[]);
    descriptor: ([Ljava/lang/String;)V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=2, locals=4, args_size=1
         0: new           #7                  // class cjj/jvm/StaticDispatch$Man
         3: dup
         4: invokespecial #8                  // Method cjj/jvm/StaticDispatch$Man."<init>":()V
         7: astore_1
         8: new           #9                  // class cjj/jvm/StaticDispatch$Woman
        11: dup
        12: invokespecial #10                 // Method cjj/jvm/StaticDispatch$Woman."<init>":()V
        15: astore_2
        16: new           #11                 // class cjj/jvm/StaticDispatch
        19: dup
        20: invokespecial #12                 // Method "<init>":()V
        23: astore_3
        24: aload_3
        25: aload_1
        26: invokevirtual #13                 // Method sayHello:(Lcjj/jvm/StaticDispatch$Human;)V
        29: aload_3
        30: aload_2
        31: invokevirtual #13                 // Method sayHello:(Lcjj/jvm/StaticDispatch$Human;)V
        34: return
      LineNumberTable:
        line 24: 0
        line 25: 8
        line 26: 16
        line 27: 24
        line 28: 29
        line 29: 34
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      35     0  args   [Ljava/lang/String;
            8      27     1   man   Lcjj/jvm/StaticDispatch$Human;
           16      19     2 woman   Lcjj/jvm/StaticDispatch$Human;
           24      11     3    sr   Lcjj/jvm/StaticDispatch;
}
SourceFile: "StaticDispatch.java"
InnerClasses:
     static #15= #9 of #11; //Woman=class cjj/jvm/StaticDispatch$Woman of class cjj/jvm/StaticDispatch
     static #17= #7 of #11; //Man=class cjj/jvm/StaticDispatch$Man of class cjj/jvm/StaticDispatch
     static abstract #19= #18 of #11; //Human=class cjj/jvm/StaticDispatch$Human of class cjj/jvm/StaticDispatch

 */