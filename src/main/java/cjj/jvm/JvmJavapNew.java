package cjj.jvm;

/**
 * @author chenjunjie
 * @since 2018-03-28
 */
public class JvmJavapNew {
    public void say(){
        Hello hi = new Hello();
    }
}



/** 反编译结果
 *
Classfile /F:/cjj/cjjwork/my_java_test/output/production/my_java_test/cjj/jvm/JvmJavapNew.class
  Last modified 2018-3-28; size 406 bytes
  MD5 checksum 4b78a9de1cbf6310838420b51e433d95
  Compiled from "JvmJavapNew.java"
public class cjj.jvm.JvmJavapNew
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #5.#18         // java/lang/Object."<init>":()V
   #2 = Class              #19            // cjj/jvm/Hello
   #3 = Methodref          #2.#18         // cjj/jvm/Hello."<init>":()V
   #4 = Class              #20            // cjj/jvm/JvmJavapNew
   #5 = Class              #21            // java/lang/Object
   #6 = Utf8               <init>
   #7 = Utf8               ()V
   #8 = Utf8               Code
   #9 = Utf8               LineNumberTable
  #10 = Utf8               LocalVariableTable
  #11 = Utf8               this
  #12 = Utf8               Lcjj/jvm/JvmJavapNew;
  #13 = Utf8               say
  #14 = Utf8               hi
  #15 = Utf8               Lcjj/jvm/Hello;
  #16 = Utf8               SourceFile
  #17 = Utf8               JvmJavapNew.java
  #18 = NameAndType        #6:#7          // "<init>":()V
  #19 = Utf8               cjj/jvm/Hello
  #20 = Utf8               cjj/jvm/JvmJavapNew
  #21 = Utf8               java/lang/Object
{
  public cjj.jvm.JvmJavapNew();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: return
      LineNumberTable:
        line 7: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/JvmJavapNew;

  public void say();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=1
         0: new           #2                  // class cjj/jvm/Hello
         3: dup
         4: invokespecial #3                  // Method cjj/jvm/Hello."<init>":()V
         7: astore_1
         8: return
      LineNumberTable:
        line 9: 0
        line 10: 8
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       9     0  this   Lcjj/jvm/JvmJavapNew;
            8       1     1    hi   Lcjj/jvm/Hello;
}
SourceFile: "JvmJavapNew.java"

 */