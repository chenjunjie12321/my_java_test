package cjj.jvm;

/**
 * JvmDeme1 Class文件字节码的工具javap测试
 * 测试内容：常量池，字段表集合，访问标志，方法表集合等
 * 先编译文件后，编译后得到JvmDeme1.class，到此文件目录下执行：javap -verbose JvmDeme1.class
 * @author chenjunjie
 * @since 2018-03-27
 */
public class JvmDeme1 {
    private int a;
    private int b;

    public JvmDeme1(){}

    public int functionInt(){
        int m = 1;
        int n = 3;
        int p = m + n ;
        return m;
    }

    public void functionVoid(){
        int k=16;
        int j=22;
    }

    public String  functionString(){
        return "hello";
    }

}


/** 反编译后的内容如下：
 *
Classfile /F:/cjj/cjjwork/my_java_test/output/production/my_java_test/cjj/jvm/JvmDeme1.class
  Last modified 2018-3-27; size 657 bytes
  MD5 checksum 4cec8924f39e61d268432a7e070a4cff
  Compiled from "JvmDeme1.java"
public class cjj.jvm.JvmDeme1
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #4.#27         // java/lang/Object."<init>":()V
   #2 = String             #28            // hello
   #3 = Class              #29            // cjj/jvm/JvmDeme1
   #4 = Class              #30            // java/lang/Object
   #5 = Utf8               a
   #6 = Utf8               I
   #7 = Utf8               b
   #8 = Utf8               <init>
   #9 = Utf8               ()V
  #10 = Utf8               Code
  #11 = Utf8               LineNumberTable
  #12 = Utf8               LocalVariableTable
  #13 = Utf8               this
  #14 = Utf8               Lcjj/jvm/JvmDeme1;
  #15 = Utf8               functionInt
  #16 = Utf8               ()I
  #17 = Utf8               m
  #18 = Utf8               n
  #19 = Utf8               p
  #20 = Utf8               functionVoid
  #21 = Utf8               k
  #22 = Utf8               j
  #23 = Utf8               functionString
  #24 = Utf8               ()Ljava/lang/String;
  #25 = Utf8               SourceFile
  #26 = Utf8               JvmDeme1.java
  #27 = NameAndType        #8:#9          // "<init>":()V
  #28 = Utf8               hello
  #29 = Utf8               cjj/jvm/JvmDeme1
  #30 = Utf8               java/lang/Object
{
  public cjj.jvm.JvmDeme1();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: return
      LineNumberTable:
        line 13: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/JvmDeme1;

  public int functionInt();
    descriptor: ()I
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=4, args_size=1
         0: iconst_1
         1: istore_1
         2: iconst_3
         3: istore_2
         4: iload_1
         5: iload_2
         6: iadd
         7: istore_3
         8: iload_1
         9: ireturn
      LineNumberTable:
        line 16: 0
        line 17: 2
        line 18: 4
        line 19: 8
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      10     0  this   Lcjj/jvm/JvmDeme1;
            2       8     1     m   I
            4       6     2     n   I
            8       2     3     p   I

  public void functionVoid();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=3, args_size=1
         0: bipush        16
         2: istore_1
         3: bipush        22
         5: istore_2
         6: return
      LineNumberTable:
        line 23: 0
        line 24: 3
        line 25: 6
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       7     0  this   Lcjj/jvm/JvmDeme1;
            3       4     1     k   I
            6       1     2     j   I

  public java.lang.String functionString();
    descriptor: ()Ljava/lang/String;
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: ldc           #2                  // String hello
         2: areturn
      LineNumberTable:
        line 28: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       3     0  this   Lcjj/jvm/JvmDeme1;
}
SourceFile: "JvmDeme1.java"

 *
 */