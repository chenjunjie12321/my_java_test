package cjj.jvm;

/**
 * @author chenjunjie
 * @since 2018-04-27
 */
public class Test {
    public static void main(String[] args) {
        SingleTon singleTon = SingleTon.getInstance();
        System.out.println("count1=" + singleTon.count1);
        System.out.println("count2=" + singleTon.count2);


        System.out.println("a =" + Test1.a);
        // 避免通过一个类的对象引用访问此类的静态变量或静态方法,无谓增加编译器解析成本,
        Test1 t = new Test1();
        System.out.println("a =" + t.a);

        try {
            Class clasz = ClassLoader.class.getClassLoader().loadClass("cjj.jvm.Hello");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
