package cjj.jvm;

/**
 * @author chenjunjie
 * @since 2018-04-03
 */
public class JvmStaticReference {
    private static int a = 1;
    public static int c = 5;

    public static void staticMethod1(){
        int i = 2;
        int j = 3;
        int m = i+j;
        int n = a;
    }

    public static void staticMethod2(){
        staticMethod1();
    }


    public void foo() {
        bar();
    }

    public void bar() { }
}


/** 编译结果
 *
Classfile /F:/cjj/cjjwork/my_java_test/output/production/my_java_test/cjj/jvm/JvmStaticReference.class
  Last modified 2018-4-3; size 768 bytes
  MD5 checksum c3981e88569d2c6fe37c20542fa825a5
  Compiled from "JvmStaticReference.java"
public class cjj.jvm.JvmStaticReference
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #7.#29         // java/lang/Object."<init>":()V
   #2 = Fieldref           #6.#30         // cjj/jvm/JvmStaticReference.a:I
   #3 = Methodref          #6.#31         // cjj/jvm/JvmStaticReference.staticMethod1:()V
   #4 = Methodref          #6.#32         // cjj/jvm/JvmStaticReference.bar:()V
   #5 = Fieldref           #6.#33         // cjj/jvm/JvmStaticReference.c:I
   #6 = Class              #34            // cjj/jvm/JvmStaticReference
   #7 = Class              #35            // java/lang/Object
   #8 = Utf8               a
   #9 = Utf8               I
  #10 = Utf8               c
  #11 = Utf8               <init>
  #12 = Utf8               ()V
  #13 = Utf8               Code
  #14 = Utf8               LineNumberTable
  #15 = Utf8               LocalVariableTable
  #16 = Utf8               this
  #17 = Utf8               Lcjj/jvm/JvmStaticReference;
  #18 = Utf8               staticMethod1
  #19 = Utf8               i
  #20 = Utf8               j
  #21 = Utf8               m
  #22 = Utf8               n
  #23 = Utf8               staticMethod2
  #24 = Utf8               foo
  #25 = Utf8               bar
  #26 = Utf8               <clinit>
  #27 = Utf8               SourceFile
  #28 = Utf8               JvmStaticReference.java
  #29 = NameAndType        #11:#12        // "<init>":()V
  #30 = NameAndType        #8:#9          // a:I
  #31 = NameAndType        #18:#12        // staticMethod1:()V
  #32 = NameAndType        #25:#12        // bar:()V
  #33 = NameAndType        #10:#9         // c:I
  #34 = Utf8               cjj/jvm/JvmStaticReference
  #35 = Utf8               java/lang/Object
{
  public static int c;
    descriptor: I
    flags: ACC_PUBLIC, ACC_STATIC

  public cjj.jvm.JvmStaticReference();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: return
      LineNumberTable:
        line 7: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/JvmStaticReference;

  public static void staticMethod1();
    descriptor: ()V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=2, locals=4, args_size=0
         0: iconst_2
         1: istore_0
         2: iconst_3
         3: istore_1
         4: iload_0
         5: iload_1
         6: iadd
         7: istore_2
         8: getstatic     #2                  // Field a:I
        11: istore_3
        12: return
      LineNumberTable:
        line 12: 0
        line 13: 2
        line 14: 4
        line 15: 8
        line 16: 12
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            2      11     0     i   I
            4       9     1     j   I
            8       5     2     m   I
           12       1     3     n   I

  public static void staticMethod2();
    descriptor: ()V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=0, locals=0, args_size=0
         0: invokestatic  #3                  // Method staticMethod1:()V
         3: return
      LineNumberTable:
        line 19: 0
        line 20: 3

  public void foo();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokevirtual #4                  // Method bar:()V
         4: return
      LineNumberTable:
        line 24: 0
        line 25: 4
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcjj/jvm/JvmStaticReference;

  public void bar();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=0, locals=1, args_size=1
         0: return
      LineNumberTable:
        line 27: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       1     0  this   Lcjj/jvm/JvmStaticReference;

  static {};
    descriptor: ()V
    flags: ACC_STATIC
    Code:
      stack=1, locals=0, args_size=0
         0: iconst_1
         1: putstatic     #2                  // Field a:I
         4: iconst_5
         5: putstatic     #5                  // Field c:I
         8: return
      LineNumberTable:
        line 8: 0
        line 9: 4
}
SourceFile: "JvmStaticReference.java"

 */