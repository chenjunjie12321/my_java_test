package cjj.proxy.jdk;

/**
 * @author chenjunjie
 * @since 2018-05-09
 */
public class HelloWorldImpl implements HelloWorld {

    @Override
    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }


}
