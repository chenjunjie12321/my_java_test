package cjj.proxy.jdk;

/**
 * @author chenjunjie
 * @since 2018-05-09
 */
public interface UserMapper {
    /**
     * 通过 id 查询用户
     * @param id
     * @return
     */
    User selectByid(Long id);

}
