package cjj.proxy.jdk;

/**
 * @author chenjunjie
 * @since 2018-05-09
 */
public interface HelloWorld {
    void sayHello(String name);
}
