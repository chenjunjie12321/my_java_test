package cjj.proxy.jdk;

import java.util.Date;

/**
 * @author chenjunjie
 * @since 2018-05-09
 */
public class UserMapperImpl implements UserMapper,HelloWorld{

    @Override
    public User selectByid(Long id) {
        System.out.println("到数据库查询数据" );
        // 这里直接使用set, 模拟是从数据库查到的数据
        User user = new User();
        user.setId(id);
        user.setUserName ("test");
        user.setUserPassword ("123456");
        user.setUserEmail ("test@mybatis.jj");
        user.setUserInfo ("test_info") ;
        user.setHeadImg(new byte[]{1,2,3});
        user.setCreateTime(new Date());
        return user;
    }

    @Override
    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }
}
