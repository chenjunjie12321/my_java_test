package cjj.proxy.staticProxy;

/**
 * @author chenjunjie
 * @since 2018-05-10
 */
public class MainTest {
    public static void main(String[] args) {
        Proxy proxy = new Proxy(new RealObject());
        proxy.doSomething();
    }
}
