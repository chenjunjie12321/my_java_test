package cjj.proxy.staticProxy;

/**
 * @author chenjunjie
 * @since 2018-05-10
 */
public class Proxy implements Action{
    private RealObject realObject;

    public Proxy(RealObject realObject) {
        this.realObject = realObject;
    }

    @Override
    public void doSomething() {
        System.out.println("before operate......");
        this.realObject.doSomething();
        System.out.println("after operate......");
    }
}
