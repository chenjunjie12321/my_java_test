package cjj.proxy.staticProxy;

/**
 * @author chenjunjie
 * @since 2018-05-10
 */
public class RealObject implements Action {
    @Override
    public void doSomething() {
        System.out.println("do something");
    }
}
