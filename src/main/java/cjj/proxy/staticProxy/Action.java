package cjj.proxy.staticProxy;

/**
 * @author chenjunjie
 * @since 2018-05-10
 */
public interface Action {
    void doSomething();
}
