package cjj.proxy.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created by youaijj on 2018/5/12.
 */
public class CglibDemo implements MethodInterceptor {

    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("Before invocation");
        methodProxy.invokeSuper(o, objects);
        System.out.println("After invocation");
        return null;
    }
}
