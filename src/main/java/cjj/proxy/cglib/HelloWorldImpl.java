package cjj.proxy.cglib;

/**
 * Created by youaijj on 2018/5/12.
 */
public class HelloWorldImpl {

    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }
}
