package cjj.proxy.cglib;

import net.sf.cglib.core.DefaultGeneratorStrategy;

/**
 * Created by youaijj on 2018/5/12.
 */
public class MainTest {
    public static void main(String[] args) {
        HelloWorldCglib cglib = new HelloWorldCglib();
        HelloWorldImpl proxy = (HelloWorldImpl)cglib.getInstance(new HelloWorldImpl());
        proxy.sayHello("cjj");

        try {
            createProxyClassFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: 2018/5/12  查找生成的代理类文件能实现
    private static void createProxyClassFile() throws Exception {
        DefaultGeneratorStrategy strategy = new DefaultGeneratorStrategy();
        //HelloWorldCglib cglib = new HelloWorldCglib();
        //Enhancer enhancer = new Enhancer();
        //enhancer.setSuperclass(new HelloWorldImpl().getClass());
        //enhancer.setCallback(cglib);
        //byte[] data = strategy.generate(enhancer);
    }

}
