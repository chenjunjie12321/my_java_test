package cjj.file;

import java.io.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.currentThread;

/**
 * @author chenjunjie
 * @since 2018-06-05.
 */
public class ReadStr {
    public static void main(String[] args) {
//        //Executors:
//        // newCachedThreadPool
//        // newFixedThreadPool
//        // newScheduledThreadPool
//
//        // 定义newFixedThreadPool，返回ExecutorService
//        int corePoolSize = 100;
//        int maximumPoolSize = corePoolSize;
//        long keepAliveTime = 0L;
//        TimeUnit unit = TimeUnit.MILLISECONDS;
//        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();
//        ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,keepAliveTime, unit, workQueue);
//
//        int threadNum = 10000;
//        // 创建线程
//        for (int i = 0; i < threadNum; i++) {
//            executor.execute(
//                    new Runnable() {
//                        @Override
//                        public void run() {
//                            ReadStr.RandomAccessFileTest();
//                        }
//                    }
//            );
//        }
//
//        // 不断监听
//        // 如果关闭后所有任务都已完成，isTerminated则返回true。注意，除非首先调用shutdown或shutdownNow，否则isTerminated永不为true。
//        executor.shutdown();
//        while(true){
//            if(executor.isTerminated()){
//                System.out.println("所有的子线程都结束了！");
//                break;
//            }
//        }


        rea();
    }


    private static void RandomAccessFileTest() {
        RandomAccessFile raf = null;
        try {
                String str = Thread.currentThread().toString()+"\n";
                byte[] bys = str.getBytes();
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                raf = new RandomAccessFile("D:\\helloll.txt", "rw");
                raf.seek(raf.length());
                raf.write(bys);
                //System.out.println("helllllllll");
            //}
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void rea() {
        StringBuffer sb = new StringBuffer("");
        FileReader reader = null;
        try {
            reader = new FileReader("D:\\ramdomAcc.txt");
            BufferedReader br = new BufferedReader(reader);
            String str = null;


            FileOutputStream out = null;
            out = new FileOutputStream(new File("D:\\authcode.txt"));
            while ((str = br.readLine()) != null) {
                String data = str;
                if(!data.contains("&code=")){
                    continue;
                }
//                JSONObject data_obj = JSONObject.parseObject(data);
//                if(!(data_obj.get("code").toString()).equals("0")){
//                    continue;
//                }
//                String redirectUrl = ((JSONObject)data_obj.get("data")).get("redirectUrl").toString();
                //System.out.println(redirectUrl);
                String regex = "code=(.*)&";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(data);//匹配类
                while (matcher.find()) {
                    //System.out.println(matcher.group(1));//打印中间字符
                    String result = matcher.group(1)+","+"\n";
                    out.write(result.getBytes());
                }
            }
        }
        catch(FileNotFoundException e){
                e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
