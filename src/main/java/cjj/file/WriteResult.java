//package cjj.file;
//
//import jdk.nashorn.internal.runtime.regexp.joni.constants.Arguments;
//import org.apache.jmeter.protocol.java.sampler.JavaSamplerClient;
//import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
//import org.apache.jmeter.samplers.SampleResult;
//
///**
// * @author chenjunjie
// * @since 2018-06-04
// */
//public class WriteResult implements JavaSamplerClient {
//    private SampleResult results;
//    private String senCode;
//    private String threadNumber;
//
//    // 设置从jmeter传入参数
//    public Arguments getDefaultParameters() {
//        Arguments params = new Arguments();
//        params.addArgument("senCode", "0");// 设置senCode参数
//        params.addArgument("threadNumber", "0");// 设置threadNumber
//        return params;
//    }
//
//    // 初始化方法，性能测试时只执行一次
//    public void setupTest(JavaSamplerContext arg0) {
//
//    }
//
//    // 重复执行测试的地方
//    public SampleResult runTest(JavaSamplerContext arg0) {
//        senCode = arg0.getParameter("senCode"); // 获取jmeter传入的参数值，
//        threadNumber = arg0.getParameter("threadNumber"); // 获取jmeter传入的参数值，
//        results = new SampleResult();
//        results.sampleStart();// jmeter 开始统计响应时间标记
//
//        AppendFile.appendFile("E:\\jmeter\\"+threadNumber+".txt", senCode);
//
//        if(senCode.length() ==8){
//            results.setSuccessful(true);
//            results.setResponseData("threadNumber:"+threadNumber+"|senCode:"+senCode, null);
//        }else{
//            results.setSuccessful(false);
//            results.setResponseData("threadNumber:"+threadNumber+"|没有获取到验证码    ", null);
//        }
//
//        results.sampleEnd();// jmeter 结束统计响应时间标记
//
//        return results;
//    }
//
//    // 结束方法，实际运行时每个线程仅执行一次，在测试方法运行结束后执行
//    public void teardownTest(JavaSamplerContext arg0) {
//    }
//
//    public static void main(String[] args) {
//        // TODO Auto-generated method stub
//    }
//}
