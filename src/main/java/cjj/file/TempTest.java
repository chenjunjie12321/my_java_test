package cjj.file;

import com.alibaba.fastjson.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chenjunjie
 * @since 2018-06-04
 */
public class TempTest {
    public static void main(String[] args) {
        String data = "{\"code\":\"0\",\"message\":\"SUCCESS\",\"data\":{\"redirectUrl\":\"http://axwl.portal.gome.work/portal/user/queryUserInfoAndMenu?_gome=0.2519392336871291&code=43eeb9d2e284c586382d4acf5c69ec3d&status=532cd5d0-b7ed-42fb-a4ab-6f3ae5531351\"}}";
        JSONObject data_obj = JSONObject.parseObject(data);
        String redirectUrl = ((JSONObject)data_obj.get("data")).get("redirectUrl").toString();
        System.out.println(redirectUrl);
        String regex = "code=(.*)&";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(redirectUrl);//匹配类
        while (matcher.find()) {
            System.out.println(matcher.group(0));
            System.out.println(matcher.group(1));//打印中间字符
        }

    }
}
