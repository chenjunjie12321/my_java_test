package cjj.observe;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * 购物车(被观察的对象，购物车添加了商品及时通知处理)
 * Created by youaijj on 2018/5/12.
 */
public class ShoppingCart extends Observable {
    private List<Product> productList = new ArrayList<Product>();;

    public ShoppingCart(){}

    public void addSomething(Product product) {
        productList.add(product);
        // 关键点
        // 设置被观察对象发生变化
        this.setChanged();
        // 通知观察者
        this.notifyObservers(product);
    }
}
