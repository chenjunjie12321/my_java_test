package cjj.observe;


public class MainTest {
    public static void main(String[] args) {
        ShoppingCart shoppingCart = new ShoppingCart();
        EmailObserver emailObserver = new EmailObserver();
        MsgObserver msgObserver = new MsgObserver();

        shoppingCart.addObserver(emailObserver);
        shoppingCart.addObserver(msgObserver);

        // 触发
        Product p = new Product();
        p.setName("女童夏日格子裙");
        p.setPrice(349.0f);
        shoppingCart.addSomething(p);
    }
}
