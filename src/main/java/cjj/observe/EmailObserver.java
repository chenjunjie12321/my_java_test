package cjj.observe;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by youaijj on 2018/5/12.
 */
public class EmailObserver implements Observer {
    @Override
    public void update(Observable o, Object product) {
        Product newProduct = (Product) product;
        System.out.println("发送邮件 --> 您的购物车中添加了商品: "+newProduct.getName());
    }
}
