package cjj.email;

import io.github.biezhi.ome.OhMyEmail;

import javax.mail.MessagingException;

import static io.github.biezhi.ome.OhMyEmail.SMTP_QQ;


/**
 * @author chenjunjie
 * @since 2018-07-09
 */
public class OhMyEmailDemo {
    public static void main(String[] args) {
        // 配置，一次即可
        OhMyEmail.config(SMTP_QQ(false), "1131012589@qq.com", "lpezkmilymfjbafh");

        try {
            OhMyEmail.subject("这是一封测试TEXT邮件")
                    .from("cjj的QQ邮箱")
                    .to("youaijj@163.com")
                    .text("信件内容：hello hello")
                    .send();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
