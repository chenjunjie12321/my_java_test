package CountDownLatchTest;

/**
 * @author chenjunjie
 * @since 2018-01-17
 */
public class MainTest {
    public static void main(String[] args) {
        boolean result = false;
        try {
            result = ApplicationStartupUtil.checkExternalServices();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("External services validation completed !! Result was :: "+ result);
    }

}
