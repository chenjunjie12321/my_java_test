package CountDownLatchTest;

import java.util.concurrent.CountDownLatch;

/**
 * @author chenjunjie
 * @since 2018-01-17
 */
public class DatabaseHealthChecker extends BaseHealthChecker
{
    public DatabaseHealthChecker(CountDownLatch latch)  {
        super("Datebase Service", latch);
    }

    @Override
    public void verifyService()
    {
        System.out.println("Checking " + this.getServiceName());
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        System.out.println(this.getServiceName() + " is UP");
    }
}