package Timer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author chenjunjie
 * @since 2018-03-15
 */
public class ScheduleExecutorServiceTest {

    public static void main(String[] args) {
        ScheduleExecutorServiceTest test = new ScheduleExecutorServiceTest();
        //test.testWithFixedDelay();
        //test.testAtFixedRate();

        //test.testTimeScheduleAtFixeRate();

        test.testTimeSchedule();
        //test.testTimeSchedules();
    }

    private ScheduledExecutorService executor;

    public ScheduleExecutorServiceTest() {
        executor = Executors.newScheduledThreadPool(4);
    }


    public void testSchedule() {
        executor.schedule(new Runnable() {

            public void run() {
                System.out.println("====");
                System.out.println("run "+ System.currentTimeMillis());
                try {
                    Thread.sleep(4000);
                    System.out.println("执行完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 3000, TimeUnit.MILLISECONDS);
    }
    public void testAtFixedRate() {
        executor.scheduleAtFixedRate(new Runnable() {

            public void run() {
                System.out.println("====");
                System.out.println("run "+ System.currentTimeMillis());
                try {
                    Thread.sleep(4000);
                    System.out.println("执行完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 1000, 3000, TimeUnit.MILLISECONDS);
    }

    public void testWithFixedDelay() {
        executor.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                System.out.println("====");
                System.out.println("run "+ System.currentTimeMillis());
                try {
                    //int i = 1 / 0;
                    Thread.sleep(300);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*
                try {
                    Thread.sleep(10000);
                    System.out.println("执行完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                */
            }
        }, 1000, 300, TimeUnit.MILLISECONDS);
    }


    public void testTimeScheduleAtFixeRate(){
        Timer timer = new Timer();
        System.out.println("start "+ System.currentTimeMillis());
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println("====");
                System.out.println("run   "+ System.currentTimeMillis());
                try {
                    Thread.sleep(2000);
                    System.out.println("complete   "+ System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 1000 , 3000);
    }

    public void testTimeSchedule(){
        Timer timer = new Timer();
        System.out.println("start "+ System.currentTimeMillis());
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("====");
                System.out.println("run   "+ System.currentTimeMillis());
                try {
                    Thread.sleep(2000);
                    System.out.println("complete   "+ System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 1000 , 3000);
    }

    public void testTimeSchedules(){
        Timer timer = new Timer();
        System.out.println("start "+ System.currentTimeMillis());
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("====");
                System.out.println("run   "+ System.currentTimeMillis());
                try {
                    Thread.sleep(7000);
                    System.out.println("complete   "+ System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 1000 , 3000);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("-----");
                System.out.println("----run   "+ System.currentTimeMillis());
                try {
                    Thread.sleep(4000);
                    System.out.println("-----complete   "+ System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 1000 , 2000);
    }
}
