package mqtest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenjunjie
 * @since 2018-01-08
 */
public class MapedFileQueue {


    // 各个文件
    private final List<MapedFile> mapedFiles = new ArrayList<MapedFile>();
    // 每个文件的大小
    private final int mapedFileSize;

    public MapedFileQueue(int mapedFileSize) {
        this.mapedFileSize = mapedFileSize;
    }

    public MapedFile getLastMapedFile(final long startOffset) {
        MapedFile mapedFileLast = new MapedFile("first maped file", 1000);;
        this.mapedFiles.add(mapedFileLast);
        if (startOffset > 5) {
            mapedFileLast = new MapedFile("大于5", 1000);
            this.mapedFiles.add(mapedFileLast);
            return  mapedFileLast;
        }
        mapedFileLast = this.mapedFiles.get(this.mapedFiles.size() - 1);
        return  mapedFileLast;
    }

    public int getMapedFilesLegth() {
        return  this.mapedFiles.size();
    }

    public List<MapedFile> getMapedFiles() {
        return mapedFiles;
    }
}
