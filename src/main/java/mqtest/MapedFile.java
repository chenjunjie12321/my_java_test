package mqtest;

/**
 * @author chenjunjie
 * @since 2018-01-08
 */
public class MapedFile {
    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    // 映射的文件名
    private  String fileName;
    // 映射的文件大小，定长
    private int fileSize;

    public MapedFile( String fileName, int fileSize){
        this.fileName = fileName;
        this.fileSize = fileSize;
    }


}
