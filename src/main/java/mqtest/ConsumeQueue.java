package mqtest;

import java.util.Iterator;
import java.util.List;

/**
 * @author chenjunjie
 * @since 2018-01-08
 */
public class ConsumeQueue {
    // 存储消息索引的队列
    private final MapedFileQueue mapedFileQueue;
    // 配置
    private final int mapedFileSize;

    public ConsumeQueue(final int mapedFileSize){
        this.mapedFileSize = mapedFileSize;
        this.mapedFileQueue = new MapedFileQueue(mapedFileSize);
    }


    private boolean putMessagePostionInfo(final long startOffset) {
        MapedFile mapedFile = this.mapedFileQueue.getLastMapedFile(startOffset);
        mapedFile.setFileName("new file name");
        mapedFile.setFileSize(299);
        return false;
    }

    public static void main(String[] args) {
        ConsumeQueue cq = new ConsumeQueue(2);
        System.out.printf("之前mapedFileList中MapedFile数量：%d\n",cq.mapedFileQueue.getMapedFilesLegth());
        cq.putMessagePostionInfo(6);
        System.out.printf("当前mapedFileList中MapedFile数量：%d\n",cq.mapedFileQueue.getMapedFilesLegth());

        Iterator it = cq.mapedFileQueue.getMapedFiles().iterator();
        while (it.hasNext()){
            MapedFile mpf = (MapedFile)it.next();
            System.out.printf("name=%s, size=%d \n",mpf.getFileName(),mpf.getFileSize());
        }
        System.out.println();
    }

}
