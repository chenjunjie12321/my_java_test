package TheadConsumerAndProducerTest;


import java.util.concurrent.locks.ReentrantLock;

/**
 * @author chenjunjie
 * @since 2018-02-11
 */
public class ConAndProMain {

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock(false); //默认为不公平锁
        ConsumerAndProducer service = new ConsumerAndProducer(lock);
        TheadConsumer th_c = new TheadConsumer(service);
        TheadProducer th_p = new TheadProducer(service);
        Thread th_1 = new Thread(th_c);
        Thread th_2 = new Thread(th_p);
        th_1.start();
        th_2.start();

    }
}
