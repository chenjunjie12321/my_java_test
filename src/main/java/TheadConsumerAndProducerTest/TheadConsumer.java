package TheadConsumerAndProducerTest;

/**
 * @author chenjunjie
 * @since 2018-02-11
 */
public class TheadConsumer implements Runnable {
    private ConsumerAndProducer service;
    private int count = 0;

    public TheadConsumer(ConsumerAndProducer consumer){
        this.service = consumer;
    }
    @Override
    public void run() {
        while (count <200) {
            this.service.consume();
            // 如果lock为公平锁，下面可以注释，如果为了达到lock为非公平锁的随机效果，可以执行如下几行代码
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            count++;
        }
    }
}
