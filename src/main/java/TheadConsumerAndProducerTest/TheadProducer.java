package TheadConsumerAndProducerTest;

/**
 * @author chenjunjie
 * @since 2018-02-11
 */
public class TheadProducer implements Runnable {
    private ConsumerAndProducer service;
    private int count = 0;

    public TheadProducer(ConsumerAndProducer consumer){
        this.service = consumer;
    }

    @Override
    public void run() {
        while (count<200) {
            this.service.produce();
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            count++;
        }
    }
}
