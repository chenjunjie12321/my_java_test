package Base;

/**
 * @author chenjunjie
 * @since 2018-01-30
 */
public class Main {
    public static void main(String[] args){
        Class clazz = InnerClassDemo.class;
        Class innerClazz[] = clazz.getDeclaredClasses();
        for (Class cls : innerClazz) {
            System.out.println(cls.getName());
        }
    }
}
