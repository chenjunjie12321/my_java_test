package Base;


/**
 * @author chenjunjie
 * @since 2018-01-30
 */
public class InnerClassDemo {
    public InnerClassDemo() { }

    private class InnerA implements MetricMBean{
        private String f = InnerA.class.getSimpleName();
        public InnerA() { }


        @Override
        public String objectName() {
            return null;
        }
    }

    private static class InnerB {
        private String f = InnerB.class.getSimpleName();
        public InnerB() {}
    }

    private Runnable r = new Runnable() {
        @Override
        public void run() {
            System.out.println("Method run of Runnable r");
        }
    };

    public interface MetricMBean
    {
        String objectName();
    }

    public interface JmxGaugeMBean extends MetricMBean
    {
        String getValue();
    }
}
