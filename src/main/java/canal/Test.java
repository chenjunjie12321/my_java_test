package canal;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;

/**
 * @author junjie.chen
 * @email junjie.chen@dmall.com
 * @date 2018/10/8
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(InstanceStatus.NEW.getValue());
        System.out.println(InstanceStatus.STOP.getValue());
        System.out.println(InstanceStatus.RUN.getValue());

        String msg = "eyJleGVjdXRpb25zIjpbeyJjb21tYW5kIjoieXVtIC1xIGNsZWFuIGFsbCBcdTAwMjZcdTAwMjYgeXVtIC1xIC15IC0tZGlzYWJsZXJlcG89KiAtLWVuYWJsZXJlcG89YXBwIGluc3RhbGwgZGF0YWh1Yi0xMDE4Iiwib3V0cHV0IjpbIlBhY2thZ2UgZGF0YWh1Yi0xMDE4LTgxLTIwMTgxMDI2MTAyMDQ1LmVsNi54ODZfNjQgYWxyZWFkeSBpbnN0YWxsZWQgYW5kIGxhdGVzdCB2ZXJzaW9uIiwiIl19XX0=";
        byte[] decode = Base64.decodeBase64(msg);
        try {
           String message = new String(decode, "UTF-8");
            System.out.println(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
