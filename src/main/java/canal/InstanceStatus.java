package canal;

/**
 * @author junjie.chen
 * @email junjie.chen@dmall.com
 * @date 2018/10/8
 */
public enum InstanceStatus {
    NEW("新建", (byte)0),
    RUN("运行中",(byte)1),
    STOP("停止",(byte)2);

    private String name;
    private Byte value;

    private InstanceStatus(String name, Byte value){
        this.name = name;
        this.value = value;
    }

    public static InstanceStatus fromTypeName(String name){
        for (InstanceStatus type : InstanceStatus.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName(){
        return this.name;
    }

    public Byte getValue(){
        return this.value;
    }
}
