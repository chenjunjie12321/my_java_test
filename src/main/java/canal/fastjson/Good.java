package canal.fastjson;


/**
 * @author junjie.chen
 * @email junjie.chen@dmall.com
 * @date 2018/10/29
 */
public class Good {
    public String name;
    public Float price;
    public String discription;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }
}
