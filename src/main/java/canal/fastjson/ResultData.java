package canal.fastjson;

/**
 * @author junjie.chen
 * @email junjie.chen@dmall.com
 * @date 2018/9/26
 */
public class ResultData<T> {
    /**
     * 响应消息
     */
    String message;

    /**
     * 响应状态
     */
    int code;

    /**
     * 响应数据
     */
    T data;

    public ResultData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
