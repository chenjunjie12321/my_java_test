package canal.fastjson;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

/**
 * @author junjie.chen
 * @email junjie.chen@dmall.com
 * @date 2018/10/29
 */
public class TypeReferenceTest {
    public static void main(String[] args) {

        String userJsonStr = "{\n" +
                "  \"message\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"name\": \"canal_adf\",\n" +
                "    \"age\": 1,\n" +
                "    \"address\": \"sichuan-chengdu-shuangliu\"\n" +
                "  },\n" +
                "  \"code\": 200\n" +
                "}";
        //下面这行注释掉第二打印出来就是true
//        ResultData<?> resultFromClass = JSONObject.parseObject(userJsonStr, new TypeReference<ResultData>() {
//        });
//        System.out.println(resultFromClass.getData() instanceof JSONObject);
        ResultData<?> userResult = JSONObject.parseObject(userJsonStr, new TypeReference<ResultData<User>>() {}.getType());
        System.out.println(userResult.getData() instanceof User);


        String goodStr = "{\n" +
                "  \"message\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"name\": \"naifen\",\n" +
                "    \"price\": 12.34,\n" +
                "    \"discription\": \"for child\"\n" +
                "  },\n" +
                "  \"code\": 200\n" +
                "}";

        ResultData<?> goodResult = JSONObject.parseObject(goodStr, new TypeReference<ResultData<Good>>() {}.getType());
        System.out.println(goodResult.getData() instanceof Good);
    }
}
