package AnnotationTest.Demo1.annotations;

import java.lang.annotation.*;

/**
 * @author chenjunjie
 * @since 2018-04-28
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Addr {
    String value() default "";
}
