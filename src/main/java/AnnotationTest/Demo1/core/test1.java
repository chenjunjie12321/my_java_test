package AnnotationTest.Demo1.core;

import AnnotationTest.Demo1.annotations.User;

import java.lang.annotation.Annotation;

/**
 * @author chenjunjie
 * @since 2018-04-28
 */
public class test1 {
    public static void main(String[] args) throws ClassNotFoundException{
        Class<?> classTest=Class.forName("AnnotationTest.Demo1.pojo.test");
        Annotation[] ann=classTest.getAnnotations();
        for(Annotation aa:ann){
            System.out.println(aa.toString());
            User u=(User)aa;
            System.out.println(u.name());
        }
    }
}
