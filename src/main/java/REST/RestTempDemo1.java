package REST;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenjunjie
 * @since 2018-05-10
 */
public class RestTempDemo1 {
    public static void main(String[] args) {
        new RestTempDemo1().test1();
    }


    public String test1(){
        String url = "http://10.122.2.235:8080/account/login";
        Map<String, Object> formMap = new HashMap<String, Object>();
        formMap.put("userId", 1);
        RestTemplate restTemplate = new RestTemplate();
        org.springframework.util.MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
        for (Map.Entry<String, Object> entry : formMap.entrySet()) {
            form.add(entry.getKey(), String.valueOf(entry.getValue()));
        }
        try {
            String response = restTemplate.postForObject(url, form, String.class);
            System.out.printf("-Post to url: %s with response: %s", url, response);
            return response;
        } catch (Exception e) {
            System.out.printf("访问url:%s, 异常", url);
            e.printStackTrace();
            return null;
        }
    }
}
