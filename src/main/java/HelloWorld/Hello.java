package HelloWorld;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chenjunjie
 * @since 2018-03-16
 */
public class Hello {
    private static final int threadNum = 1000;
    static int s = 0;
    private static CountDownLatch latch = new CountDownLatch(threadNum);

    public static void main(String[] args) {

        //        String REPLACE  = "输出：";
        //        String REGEX = "Stdout";
        //        Pattern p = Pattern.compile(REGEX);
        //
        //
        //        String resultStr =" install process succeeded Stdout : Package datahub-1014-97-20181106175315.el6.x86_64 already installed and latest version "
        //               + "Stderr : Failed to set locale, defaulting to C";
        //
        //        Matcher m = p.matcher(resultStr);
        //        resultStr = m.replaceAll(REPLACE);
        //
        //
        //        REPLACE  = "错误原因：";
        //        REGEX = "Stderr";
        //        p = Pattern.compile(REGEX);
        //        m = p.matcher(resultStr);
        //        resultStr = m.replaceAll(REPLACE);
        //
        ////        resultStr.replaceAll("Stdout*","输出：");
        ////        resultStr.replaceAll("Stderr","错误原因：");
        //
        //        System.out.println(resultStr);

        String Stdout = "Stdout :";
        String Stderr = "Stderr :";
        String hi = "Stdout :            \t            \n Stderr : Failed to set locale, defaulting to C";
        String hi1 = " uninstall process succeeded Stdout :            \t            \n Stderr : Failed to set locale, defaulting to C";
        //        String regex = "/([a-z]+)( Stdout : )([\\S]+)(Stderr : )([a-z]+)/ig/";
        String rgx = "(" + Stdout + "|" + Stderr + ")";
        String[] v = hi.split(rgx);
        boolean stdoutisEmptiy = true;
        boolean stderrisEmptiy = true;
        char[] stdoutChars = null;
        char[] stderrChars = null;
        if (v.length == 3) {
            stdoutChars = v[1].toCharArray();
            stderrChars = v[2].toCharArray();
        } else {
            if (hi.contains(Stdout)) {
                stdoutChars = v[1].toCharArray();
            }
            if (hi.contains(Stderr)) {
                stderrChars = v[1].toCharArray();
            }
        }

        for (char ch : stdoutChars) {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                stdoutisEmptiy = false;
                break;
            }
        }

        for (char ch : stderrChars) {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                stderrisEmptiy = false;
                break;
            }
        }

        String out  = "输出：";
        String err  ="错误原因：";
        StringBuffer sb = new StringBuffer();
        String result = sb.toString();
        String outStr = "";
        if (!stdoutisEmptiy) {
            sb.append(out);
            sb.append(stdoutChars);
            sb.append("\n");
        }

        if (!stderrisEmptiy) {
            sb.append(err);
            sb.append(stderrChars);
            sb.append("\n");
        }

        System.out.println(sb.toString());
        // 创建ThreadPoolExecutor线程池对象
        //        int maxPoolSize = 10000;
        //        int corePoolSize = 10;
        //        long keepActiveTime = 200;
        //        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(corePoolSize);
        //
        //
        //        ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepActiveTime, TimeUnit.SECONDS, workQueue);
        //        for(int i=0; i<threadNum;i++){
        //            executor.execute(new Runnable() {
        //                @Override
        //                public void run() {
        //                    printS();
        //                    latch.countDown();
        //                }
        //            });
        //        }
        //
        //        try {
        //            latch.await();
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }
        //
        //
        //        try {
        //            while (executor.getActiveCount() > 0) {
        //                Thread.sleep(10);
        //            }
        //
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        } finally {
        //            executor.shutdown();
        //        }
    }

    public static void printS() {
        s = 0;
        s = 3;
        System.out.println(s);
    }

    public static void test() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                printS();
            }
        }).start();
    }
}
