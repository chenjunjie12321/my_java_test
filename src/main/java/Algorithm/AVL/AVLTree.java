package Algorithm.AVL;

/**
 * @author chenjunjie
 * @since 2018-04-08
 */
public interface AVLTree {
    /**
     * 前序遍历AVL树
     */
    void preOrder(AVLTreeNode node);

    /**
     * 中序遍历AVL树
     */
    void InOrder(AVLTreeNode node);

    /**
     * 后序遍历AVL树
     */
    void postOrder(AVLTreeNode node);


    /**
     * 插入节点
     * @param value
     */
    AVLTreeNode insert(AVLTreeNode node,int value);

    /**
     * 删除节点
     * @param value
     */
    void remove(AVLTreeNode node,int value);

    /**
     * 返回AVL中的最小值
     * @return
     */
    int minimum(AVLTreeNode node);

    /**
     * 返回AVL中的最大值
     * @return
     */
    int maximum(AVLTreeNode node);

    /**
     * 返回树的高度
     * 平衡因子 = 左子树高度 - 右子树高度
     * @return
     */
    int height(AVLTreeNode node);

    /**
     * 左旋转
     * @param node
     * @return
     */
    AVLTreeNode leftRotation(AVLTreeNode node);

    /**
     * 右旋转
     * @param node
     * @return
     */
    AVLTreeNode rightRotation(AVLTreeNode node);

    /**
     * 双旋:先左旋后右旋操作
     * @param node
     * @return
     */
    AVLTreeNode leftRightRotation(AVLTreeNode node);

    /**
     * 双旋:先右旋后左旋操作
     * @param node
     * @return
     */
    AVLTreeNode rightLeftRotation(AVLTreeNode node);




}
