package Algorithm.AVL;


/**
 * @author chenjunjie
 * @since 2018-04-08
 */
public class AVLTreeNode {
    /**
     * 左孩子
     */
    public AVLTreeNode lChild;

    /**
     * 右孩子
     */
    public AVLTreeNode rChild;

    /**
     * 当前节点高度
     */
    public int height;

    /**
     * 节点数据（暂定为int型）
     */
    public int data;

    public AVLTreeNode(int data, int heigth){
        this.data = data;
        this.height = heigth;
    }

    public AVLTreeNode(int data){
       this(data, 0);
    }

}
