package Algorithm.AVL;


/**
 * @author chenjunjie
 * @since 2018-04-08
 */
public class Main {
    public static void main(String[] args){
        AVLTreeImpl at = new AVLTreeImpl();
        AVLTreeNode root = at.insert(null, 5);
        at.insert(root, 8);
        at.InOrder(root);
        System.out.println("-----中序（排序）-----");
        root = at.insert(root, 7);
        root = at.insert(root,4);
        root = at.insert(root,2);
        root = at.insert(root,3);
        at.InOrder(root);
        System.out.println("------后序------");
        at.postOrder(root);
        System.out.println("------前序------");
        at.preOrder(root);

    }
}
