package Algorithm.Linked;


import static javafx.scene.input.KeyCode.T;

/**
 * @author chenjunjie
 * @since 2018-04-25
 */
public class LinkNoed {
    public int item;
    public LinkNoed next;
    public LinkNoed prev;

    LinkNoed(LinkNoed prev, int element, LinkNoed next) {
        this.item = element;
        this.next = next;
        this.prev = prev;
    }
}
