package Algorithm.Linked;

/**
 * @author chenjunjie
 * @since 2018-04-25
 */
public interface LinkedListDemo {
    /**
     * 增加节点
     * @param data
     */
    void add(int data);


    /**
     * 在指定节点的之前节点上添加新节点
     * @param data
     * @param index
     */
    void add(int data, int index);


    /**
     * 移除队列尾部节点
     */
    void removeFirst();


    /**
     * 移除链表指定位置的数据
     * @return
     */
    int remove(int index);


    /**
     * 移除链表指定位置的数据
     * @return
     */
    int removeLast();


    /**
     * 获取链表尾部数据
     * @return
     */
    int getLast();

    /**
     * 获取链表头部数据
     * @return
     */
    int getFirst();


    /**
     * 获取链表指定位置的数据
     * @return
     */
    int get(int index);

    /**
     * 获取链表节点个数
     * @return
     */
    int getSize();


}
