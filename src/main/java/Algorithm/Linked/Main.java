package Algorithm.Linked;

/**
 * @author chenjunjie
 * @since 2018-04-25
 */
public class Main {
    public static void main(String[] args) {
        LinkedListDemo linkList = new LinkedListImpl();
        linkList.add(23);
        linkList.add(12);
        linkList.add(38);

        linkList.add(54);
        linkList.add(17);
        linkList.add(3);

        linkList.add(42);
        linkList.add(19);
        linkList.add(35);


//        System.out.println(linkList.get(4));
//        System.out.println(linkList.get(5));
//        System.out.println(linkList.get(6));
//        System.out.println(linkList.get(7));

        linkList.add(666,4);

        System.out.println("-----------");
        System.out.println(linkList.get(3));
        System.out.println(linkList.get(4));
        System.out.println(linkList.get(5));
        System.out.println(linkList.get(6));


        System.out.println("-----------");
        System.out.println(linkList.getFirst());
        System.out.println(linkList.getLast());

        System.out.println("--size------");
        //LinkedListImpl l = (LinkedListImpl)(linkList);
        System.out.println(linkList.getSize());


    }
}
