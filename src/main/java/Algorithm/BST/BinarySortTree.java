package Algorithm.BST;

import java.util.List;

/**
 * 二叉排序树节点
 * @author chenjunjie
 * @since 2018-04-03
 */
public class BinarySortTree {
    /**
     * 左孩子
     */
    public BinarySortTree lChild;
    /**
     * 右孩子
     */
    public BinarySortTree rChild;
    /**
     * 根节点(root执行对象)
     */
//    public BinarySortTree root;
    /***
     * 数据域(暂时定为int类型)
     */
    public int data;
    /**
     * 存储所有的节点
     */
    public List<BinarySortTree> datas;

}
