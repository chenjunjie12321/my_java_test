package Algorithm.LRU;

import java.util.*;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Node;


/**
 * @author chenjunjie
 * @since 2018-04-24
 */
public class Test {
    /**
     * LRU是Least Recently Used 的缩写，即“最近最少使用”
     * 扩展 LinkedHashMap 可以实现就实现了LRU，
     * LinkedHashMap中最近读取的会放在最前面，最最不常读取的会放在最后，同时
     * 调用removeEldestEntry会移除链表中“最老”的节点，但注意源码中调用该方法返回的值为false
     * 因此，可以继承LinkedHashMap重写removeEldestEntry, 从而构建LRU缓存
     */
    LinkedHashMap l;
    ArrayList a;
    LinkedList b;

    public static void main (String[] args) {
//        LRUCache lurCache = new LRUCache(10);
//        int N = 12 ; //7
//        int i = 1;
//        while (i<N){
//            lurCache.put(i,i*2);
//            i++;
//        }
//
//        System.out.println(lurCache.get(4));
//        lurCache.put(4,666);
//
//        // 遍历map
//        Map hsMap = lurCache.getMap();
//        Iterator it = hsMap.entrySet().iterator();
//        while(it.hasNext()){
//            Map.Entry<Integer,LRUCache.Node> result = (Map.Entry<Integer,LRUCache.Node>)it.next();
//            System.out.printf("(key=%d,value=%d)\n",result.getKey(),result.getValue().val);
//        }
//
//        System.out.println("-----------");
//
//        // 遍历node
//        LRUCache.Node n = lurCache.getHead();
//        LRUCache.Node cur = n;
//        while(cur != null){
//            System.out.printf("(key=%d,value=%d)\n",cur.key,cur.val);
//            cur = cur.post;
//        }

        //LinkedHashMap lmp = new LinkedHashMap(10);
        HashMap lmp = new HashMap(10);
        int i = 1;
        int N = 9;
        while (i<N){
            lmp.put(i,i*2);
            i++;
        }

        Iterator it = lmp.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<Integer,Integer> result = (Map.Entry<Integer,Integer>)it.next();
            System.out.printf("[key=%d,value=%d]\n",result.getKey(),result.getValue());
        }

        lmp.put(2,666);

        lmp.get(1);
        it = lmp.entrySet().iterator();
        System.out.println("------");
        while(it.hasNext()){
            Map.Entry<Integer,Integer> result = (Map.Entry<Integer,Integer>)it.next();
            System.out.printf("[key=%d,value=%d]\n",result.getKey(),result.getValue());
        }



    }


}
