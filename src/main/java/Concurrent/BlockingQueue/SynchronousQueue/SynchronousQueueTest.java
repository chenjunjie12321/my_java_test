package Concurrent.BlockingQueue.SynchronousQueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

/**
 * A program tests for producer-consumer using SynchronousQueue
 *
 * @author www.codejava.net
 */
public class SynchronousQueueTest {
    static final int NUMBER_OF_CONSUMERS = 5;
    static final int NUMBER_OF_PRODUCERS = 10;

    public static void main(String[] args) {

        // BlockingQueue<Integer> syncQueue = new SynchronousQueue<>(); //默认为非公平
        BlockingQueue<Integer> syncQueue = new SynchronousQueue<>(true);

        Producer[] producers = new Producer[NUMBER_OF_PRODUCERS];
        for (int i = 0; i < NUMBER_OF_PRODUCERS; i++) {
            producers[i] = new Producer(syncQueue);
            producers[i].start();
        }

        Consumer[] consumers = new Consumer[NUMBER_OF_CONSUMERS];

        for (int i = 0; i < NUMBER_OF_CONSUMERS; i++) {
            consumers[i] = new Consumer(syncQueue);
            consumers[i].start();
        }
    }
}
